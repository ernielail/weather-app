//
//  ViewController.swift
//  Weather App
//
//  Created by Ernie Lail on 1/6/18.
//  Copyright © 2018 Ernie Lail. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var textInput: UITextField!
    
    
    
    
    @IBAction func submitSearch(_ sender: Any) {
        print("Searching")
        if((textInput.text) != nil){
            let userInput = NSString(string:textInput.text!)
            let location = userInput.replacingOccurrences(of: " ", with: "-")
            let locationFinal = location.lowercased()
            let myURL = URL(string: "https://www.weather-forecast.com/locations/"+locationFinal+"/forecasts/latest")
            print(myURL!)
            let myRequest = NSMutableURLRequest(url: myURL!)
            var message = ""
            
            let task = URLSession.shared.dataTask(with: myRequest as URLRequest){
                data, response, error in
                if error != nil{
                    //display error
                    print("There was an error!!!");
                    message = "An error occured, please try again."
                }
                else{
                    //got data
                    print("Got Data!!!");
                   
                    if let unwrappedData = data {
                        let dataString = NSString(data: unwrappedData, encoding: String.Encoding.utf8.rawValue)
                       
                        if(dataString?.contains("<span class=\"phrase\">"))!{
                            let dataArray =  dataString?.components(separatedBy: "<span class=\"phrase\">")
                            let dataArray2 =  dataArray?[1].components(separatedBy: "</span>")
                            let finalDataString = dataArray2?[0].replacingOccurrences(of: "&deg;", with: " Degrees ")
                            
                            if((dataString?.contains("error"))! || (dataString?.contains("Error"))! || (dataString?.contains("You may have mistyped the address"))!){
                                print("Returned Error to Text Field")
                                message = "An error occured, please try again."
                            }
                                
                            else{
                                print("Returned Data to Text Field")
                                message = finalDataString!
                            }
                        }
                            
                        else{
                            message = "An error occured, please try again."
                        }
                        DispatchQueue.main.sync(execute: {
                            self.returnText.text = message
                        });
                    }
                }
            }
            task.resume()
        }
        else{
            self.returnText.text = "Please enter a city in the search field."
        }
    }
    
    
    
    
    @IBOutlet var returnText: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        returnText.text = ""
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
        
    }
    
    
    
}

